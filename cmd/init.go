package cmd

import (
	"errors"
	"os"
	"path/filepath"

	"bitbucket.org/martin_kedmenec/module/cmd/prompt"
	"bitbucket.org/martin_kedmenec/module/internal"
	"bitbucket.org/martin_kedmenec/module/internal/file"
	"bitbucket.org/martin_kedmenec/module/internal/git"
	"github.com/go-git/go-billy/v5"
	"github.com/spf13/cobra"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:                   "init [path]",
	DisableFlagsInUseLine: true,
	Short:                 "Initialize a new Magento 2 module in a location of your choice.",
	Long: `Initialize a new Magento 2 module in a location of your choice.

The prompt asks for vendor and module names, a comma-seperated array of author names, and either an absolute or relative module installation path`,
	Run: func(cmd *cobra.Command, args []string) {
		// Validate command arguments
		modulePath, err := validateArgs(args)
		internal.LogError(err)

		// Parse ~/.git-credentials
		err = git.ParseGitCredentials()
		internal.LogError(err)

		// Initialize channel and run Git operation on it
		fsDone := make(chan *billy.Filesystem)
		go func(fsDone chan *billy.Filesystem) {
			storage, fs := file.NewFileSystem()
			git.CopyBoilerplateModule(storage, &fs)

			fsDone <- &fs
		}(fsDone)

		// Get all workspaces the user is a member of
		wDone := make(chan []string)
		go func(wDone chan []string) {
			workspaces, err := git.GetWorkspaces()
			internal.LogError(err)

			wDone <- workspaces
		}(wDone)

		// Run prompt
		moduleData := prompt.Run(<-wDone)

		// For testing
		// ws := <-wDone
		// moduleData := &prompt.ModuleData{Vendor: "Visma", Module: "MyTest", Description: "This is a very funny description", Authors: "Martin Kedmenec", Workspace: ws[1], Git: true}

		// Create vendor and module directory names and check if module directory already exists
		vendorName := file.CreateComposerVendorName(moduleData.Vendor)
		moduleName := file.CreateComposerModuleName(moduleData.Module)
		vendorDir := filepath.Join(modulePath, vendorName)
		moduleDir := filepath.Join(vendorDir, moduleName)
		stat, err := os.Stat(moduleDir)

		// Check if module already exists
		if stat != nil && err == nil {
			internal.LogMessageWithError("module already exists", err)
		}

		// Create module when Git operation is done
		fs := <-fsDone
		repositoryName := file.CreateModule(*fs, moduleData, moduleDir, vendorName, moduleName)

		if !moduleData.Git {
			os.Exit(0)
		}

		// Initialize Git if selected
		git.InitGit(moduleDir, moduleData.Workspace, repositoryName)

		os.Exit(0)
	},
}

func init() {
	rootCmd.AddCommand(initCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// initCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// initCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// Validate command arguments
func validateArgs(args []string) (string, error) {
	// Check if more than one argument is provided
	if len(args) > 1 {
		return "", errors.New("invalid set of arguments")
	}

	// Check if no argument or . is provided
	if len(args) == 0 || args[0] == "." {
		return ".", nil
	}

	// Check if the path argument is a valid path on the system
	stat, err := os.Stat(args[0])
	if stat == nil || err != nil {
		return "", errors.New("invalid path")
	}

	return args[0], nil
}

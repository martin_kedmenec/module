package prompt

import (
	"errors"
	"regexp"

	"bitbucket.org/martin_kedmenec/module/internal"
	"github.com/manifoldco/promptui"
)

type ModuleData struct {
	Vendor      string
	Module      string
	Description string
	Authors     string
	Workspace   string
	Git         bool
}

// Prompts the user for the vendor name.
// Composer module name validation regex: ^[a-z0-9]([_.-]?[a-z0-9]+)*/[a-z0-9](([_.]?|-{0,2})[a-z0-9]+)*$
func vendorNamePrompt() string {
	validate := func(input string) error {
		allowedPattern := regexp.MustCompile(`\b[A-Z][A-Za-z0-9]*\b`)

		if !allowedPattern.MatchString(input) {
			return errors.New("invalid pattern")
		}

		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Vendor name",
		Validate: validate,
	}

	result, err := prompt.Run()

	internal.LogMessageWithError("Prompt failed", err)

	return result
}

// Prompts the user for the module name
func moduleNamePrompt() string {
	// TODO The validation function should check whether the module already exists
	validate := func(input string) error {
		allowedPattern := regexp.MustCompile(`\b[A-Z][A-Za-z0-9]*\b`)

		if !allowedPattern.MatchString(input) {
			return errors.New("invalid pattern")
		}

		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Module name",
		Validate: validate,
	}

	result, err := prompt.Run()

	internal.LogMessageWithError("Prompt failed", err)

	return result
}

// Prompts the user for the module name
func descriptionPrompt() string {
	prompt := promptui.Prompt{
		Label: "Module description",
	}

	result, err := prompt.Run()

	internal.LogMessageWithError("Prompt failed", err)

	return result
}

// Prompts the user for the authors name as comma-seperated values
func authorsPrompt() string {
	prompt := promptui.Prompt{
		Label: "Authors. Format: [Name, email], ...",
	}

	result, err := prompt.Run()

	internal.LogMessageWithError("Prompt failed", err)

	return result
}

// Prompts the user for the workspace ID
func workspacePrompt(workspaces []string) string {
	prompt := promptui.Select{
		Label:     "Select the workspace you want to initialize the remote repository in",
		Items:     workspaces,
		CursorPos: 0,
	}

	_, result, err := prompt.Run()

	internal.LogMessageWithError("Prompt failed", err)

	return result
}

// Prompts the user whether new local and remote Git repositories should be initialized, all files added, committed, and pushed to Bitbucket
func gitPrompt() bool {
	prompt := promptui.Select{
		Label:     "Do you want to initialize new local and remote Git repositories, add all files, commit, and push to Bitbucket?",
		Items:     []string{"No", "Yes"},
		CursorPos: 0,
	}

	_, result, err := prompt.Run()

	internal.LogMessageWithError("Prompt failed", err)

	return result == "Yes"
}

// Executes the prompt questions and saves them in memory.
func Run(workspaces []string) *ModuleData {
	vendor := vendorNamePrompt()
	module := moduleNamePrompt()
	description := descriptionPrompt()
	authors := authorsPrompt()
	workspace := workspacePrompt(workspaces)
	git := gitPrompt()

	return &ModuleData{vendor, module, description, authors, workspace, git}
}

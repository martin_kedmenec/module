package file

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/martin_kedmenec/module/cmd/prompt"
	"bitbucket.org/martin_kedmenec/module/internal"
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/util"
)

// Iterates over all files in the boilerplate module and replaces the placeholders with the actual values
func CreateModule(bfs billy.Filesystem, moduleData *prompt.ModuleData, moduleDir string, vendorName string, moduleName string) string { // , moduleData *prompt.ModuleData

	// Create vendor and module directories
	os.MkdirAll(moduleDir, 0755) // visma/module-test

	repositoryName := createRepositoryName(moduleData.Vendor, moduleName)

	// Iterate over each directory and file in filesystem and execute callback function
	err := util.Walk(bfs, ".", func(path string, info fs.FileInfo, err error) error {
		internal.ReturnError(err)

		// If it's a directory, create a directory
		if info.Mode().IsDir() && path != "." { // info.Mode().IsDir() = 2147484068 || 0x800001a4
			err := os.Mkdir(filepath.Join(moduleDir, path), 0755)
			internal.LogError(err)

			return nil
		}

		// Skip the root directory, files in .boilerplateignore and empty files
		if info.Name() == "/" || isIgnored(bfs, info.Name()) {
			return nil
		}

		var fileContents string

		// If the file contains data, read it and replace its contents
		if !info.Mode().IsDir() && info.Size() != 0 {
			file, err := util.ReadFile(bfs, path)
			internal.LogError(err)

			// Replace placeholders with actual values
			replacer := strings.NewReplacer(
				"VENDOR", moduleData.Vendor,
				"MAGE_MODULE", moduleData.Module,
				"COMPOSER_VENDOR", vendorName,
				"COMPOSER_MODULE", moduleName,
				"DESCRIPTION", moduleData.Description,
				"CURRENT_DATE", getCurrentDate(),
				"WORKSPACE", moduleData.Workspace,
				"REPOSITORY", repositoryName)
			fileContents = replacer.Replace(string(file))

			// Add authors
			if info.Name() == "composer.json" {
				err = addAuthors(file, moduleData.Authors)
				internal.LogError(err)
			}
		}

		// Remove .sample from file names
		fileName := strings.Replace(info.Name(), ".sample", "", 1)

		// Create matching files on host
		f, err := os.Create(filepath.Join(moduleDir, filepath.Dir(path), fileName))
		internal.LogError(err)
		defer f.Close()

		// Write to files
		f.Write([]byte(fileContents))
		internal.LogError(err)

		return nil
	})

	internal.LogError(err)

	fmt.Println("Module created:", moduleDir)

	return repositoryName
}

// Checks whether file is .boilerplateignore or ignored in .boilerplateignore
func isIgnored(bfs billy.Filesystem, fileName string) bool {
	if fileName == ".boilerplateignore" {
		return true
	}

	file, err := util.ReadFile(bfs, filepath.Base(".boilerplateignore"))
	internal.LogError(err)

	ignoredFiles := strings.Split(string(file), "\n")

	for _, b := range ignoredFiles {
		if b == fileName {
			return true
		}
	}

	return false
}

// Gets the current date. Example output: 2022-01-26
func getCurrentDate() string {
	return time.Now().Format("2006-01-02")
}

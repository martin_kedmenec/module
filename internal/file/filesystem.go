package file

import (
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-git/v5/storage/memory"
)

// Creates a new virtual filesytem in memory
func NewFileSystem() (*memory.Storage, billy.Filesystem) {
	storage := memory.NewStorage()
	fs := memfs.New()

	return storage, fs
}

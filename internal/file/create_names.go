package file

import (
	"regexp"
	"strings"
)

// Creates a Composer vendor name that conforms to the composer.json standard. Example output: visma
func CreateComposerVendorName(vendor string) string {
	return strings.ToLower(vendor)
}

// Creates a Composer module name that conforms to the composer.json standard. Example output: module-my-test
func CreateComposerModuleName(module string) string {
	joined := append([]string{"module"}, splitModuleName(module)...)

	return strings.ToLower(strings.Join(joined, "-"))
}

// Creates a repository name that conforms to the Visma Digital Commerce standard. Example output: visma-module-my-test-m2
func createRepositoryName(vendor string, composerModuleName string) string {
	return strings.Join([]string{strings.ToLower(vendor), composerModuleName, "m2"}, "-")
}

// Splits the module name into slices in case the name consists of multiple capitalized parts
func splitModuleName(module string) []string {
	re := regexp.MustCompile(`[A-Z][^A-Z]*`)

	return re.FindAllString(module, -1)
}

package file

import (
	"encoding/json"

	"bitbucket.org/martin_kedmenec/module/internal"
)

type Composer struct {
	Authors []struct {
		Name  string `json:"name"`
		Email string `json:"email"`
	} `json:"authors"`
}

func addAuthors(file []byte, authors string) error {
	var composerAuthors Composer

	err := json.Unmarshal(file, &composerAuthors)
	internal.ReturnError(err)

	return nil
}

package git

import (
	"regexp"
	"testing"
)

func TestCreateApiUrl(t *testing.T) {
	testCases := []struct {
		operation string
		slug1     string
		slug2     string
	}{
		{
			operation: "user",
			slug1:     "permissions",
			slug2:     "workspaces",
		},
		{
			operation: "hello",
			slug1:     "slug",
			slug2:     "test",
		},
		{
			operation: "1",
			slug1:     "haha",
			slug2:     "aldfkj",
		},
	}

	for _, tC := range testCases {
		apiUrl := createApiUrl(tC.operation, tC.slug1, tC.slug2)

		want := regexp.MustCompile(`\s\S*`)

		if !want.MatchString(apiUrl) {
			t.Fatal(`Url`)
		}
	}
}

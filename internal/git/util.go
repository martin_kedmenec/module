package git

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"os/user"
	"regexp"
	"strings"
	"time"

	"bitbucket.org/martin_kedmenec/module/internal"
	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	gogithttp "github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/go-git/go-git/v5/storage/memory"
)

const bitbucketBaseUrl string = "https://bitbucket.org"

const bitbucketApiBaseUrl string = "https://api.bitbucket.org/2.0"

var gitCredentials GitCredentials

type GitCredentials struct {
	Username string
	Password string
}

type Workspaces struct {
	Pagelen int `json:"pagelen"`
	Page    int `json:"page"`
	Size    int `json:"size"`
	Values  []struct {
		Type         string `json:"type"`
		Permission   string `json:"permission"`
		LastAccessed string `json:"last_accessed"`
		AddedOn      string `json:"added_on"`
		User         struct {
			Type        string `json:"type"`
			Uuid        string `json:"uuid"`
			Nickname    string `json:"nickname"`
			DisplayName string `json:"display_name"`
		} `json:"user"`
		Workspace struct {
			Type string `json:"type"`
			Uuid string `json:"uuid"`
			Slug string `json:"slug"`
			Name string `json:"name"`
		} `json:"workspace"`
	} `json:"values"`
}

type Repository struct {
	Scm     string      `json:"scm"`
	Website interface{} `json:"website"`
	HasWiki bool        `json:"has_wiki"`
	UUID    string      `json:"uuid"`
	Links   struct {
		Watchers struct {
			Href string `json:"href"`
		} `json:"watchers"`
		Branches struct {
			Href string `json:"href"`
		} `json:"branches"`
		Tags struct {
			Href string `json:"href"`
		} `json:"tags"`
		Commits struct {
			Href string `json:"href"`
		} `json:"commits"`
		Clone []struct {
			Href string `json:"href"`
			Name string `json:"name"`
		} `json:"clone"`
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
		Source struct {
			Href string `json:"href"`
		} `json:"source"`
		HTML struct {
			Href string `json:"href"`
		} `json:"html"`
		Avatar struct {
			Href string `json:"href"`
		} `json:"avatar"`
		Hooks struct {
			Href string `json:"href"`
		} `json:"hooks"`
		Forks struct {
			Href string `json:"href"`
		} `json:"forks"`
		Downloads struct {
			Href string `json:"href"`
		} `json:"downloads"`
		Pullrequests struct {
			Href string `json:"href"`
		} `json:"pullrequests"`
	} `json:"links"`
	ForkPolicy string `json:"fork_policy"`
	FullName   string `json:"full_name"`
	Name       string `json:"name"`
	Project    struct {
		Links struct {
			Self struct {
				Href string `json:"href"`
			} `json:"self"`
			HTML struct {
				Href string `json:"href"`
			} `json:"html"`
			Avatar struct {
				Href string `json:"href"`
			} `json:"avatar"`
		} `json:"links"`
		Type string `json:"type"`
		Name string `json:"name"`
		Key  string `json:"key"`
		UUID string `json:"uuid"`
	} `json:"project"`
	Language   string    `json:"language"`
	CreatedOn  time.Time `json:"created_on"`
	Mainbranch struct {
		Type string `json:"type"`
		Name string `json:"name"`
	} `json:"mainbranch"`
	Workspace struct {
		Slug  string `json:"slug"`
		Type  string `json:"type"`
		Name  string `json:"name"`
		Links struct {
			Self struct {
				Href string `json:"href"`
			} `json:"self"`
			HTML struct {
				Href string `json:"href"`
			} `json:"html"`
			Avatar struct {
				Href string `json:"href"`
			} `json:"avatar"`
		} `json:"links"`
		UUID string `json:"uuid"`
	} `json:"workspace"`
	HasIssues bool `json:"has_issues"`
	Owner     struct {
		DisplayName string `json:"display_name"`
		UUID        string `json:"uuid"`
		Links       struct {
			Self struct {
				Href string `json:"href"`
			} `json:"self"`
			HTML struct {
				Href string `json:"href"`
			} `json:"html"`
			Avatar struct {
				Href string `json:"href"`
			} `json:"avatar"`
		} `json:"links"`
		Type      string `json:"type"`
		Nickname  string `json:"nickname"`
		AccountID string `json:"account_id"`
	} `json:"owner"`
	UpdatedOn   time.Time `json:"updated_on"`
	Size        int       `json:"size"`
	Type        string    `json:"type"`
	Slug        string    `json:"slug"`
	IsPrivate   bool      `json:"is_private"`
	Description string    `json:"description"`
}

// Parses ~/.gitconfig and returns a config struct
func parseGitConfig() *config.Config {
	usr, err := user.Current()
	internal.LogError(err)

	file, err := ioutil.ReadFile(usr.HomeDir + "/.gitconfig")
	internal.LogError(err)

	cfg, err := config.ReadConfig(bytes.NewReader(file))
	internal.LogError(err)

	return cfg
}

// Parses ~/.git-credentials and assigns the username and password to gitCredentials
func ParseGitCredentials() error {
	usr, err := user.Current()
	internal.ReturnError(err)

	// Check if ~/.git-credentials exists
	_, err = os.Stat(usr.HomeDir + "/.git-credentials")
	internal.ReturnError(err)

	// Read ~/.git-credentials
	file, err := ioutil.ReadFile(usr.HomeDir + "/.git-credentials")
	internal.ReturnError(err)

	// Check if Bitbucket credentials exist
	re := regexp.MustCompile(`.*//(\S+).*:(\S+).*@(bitbucket.org)`)
	matches := re.FindStringSubmatch(string(file))

	if matches[3] != "bitbucket.org" {
		return errors.New("no Bitbucket credentials could be found")
	}

	username := matches[1]
	password := matches[2]

	gitCredentials = GitCredentials{username, password}

	return nil
}

// Creates a Bitbucket API request URL based on the operation
func createApiUrl(operation string, slug1 string, slug2 string) string {
	return strings.Join([]string{bitbucketApiBaseUrl, operation, slug1, slug2}, "/")
}

// Returns a slice containing all workspaces the user is a member of
func GetWorkspaces() ([]string, error) {
	resp, err := doRequest("GET", "user", "permissions", "workspaces")
	internal.LogError(err)

	defer resp.Body.Close()

	var respWorkspaces Workspaces

	if resp.StatusCode != http.StatusOK {
		return nil, http.ErrAbortHandler
	}

	err = json.NewDecoder(resp.Body).Decode(&respWorkspaces)

	if err != nil {
		return nil, err
	}

	var workspaces []string

	for _, w := range respWorkspaces.Values {
		workspaces = append(workspaces, w.Workspace.Slug)
	}

	return workspaces, nil
}

// Creates remote repository in Bitbucket
func createRemote(workspace string, repositoryName string) (string, string, error) {
	resp, err := doRequest("POST", "repositories", workspace, repositoryName)
	internal.LogError(err)

	defer resp.Body.Close()

	var respRepository Repository

	if resp.StatusCode != http.StatusOK {
		return "", "", http.ErrAbortHandler
	}

	err = json.NewDecoder(resp.Body).Decode(&respRepository)
	internal.LogError(err)

	var repoUrl string

	for _, c := range respRepository.Links.Clone {
		if c.Name == "https" {
			repoUrl = c.Href
			break
		}
	}

	return repoUrl, respRepository.Links.HTML.Href, nil
}

// Adds the newly created Bitbucket remote repository as a remote repository
func addRemote(r *git.Repository, repoUrl string) error {
	_, err := r.CreateRemote(&config.RemoteConfig{
		Name: "origin",
		URLs: []string{repoUrl},
	})
	internal.ReturnError(err)

	return nil
}

// Copies the boilerplate module into the virtual filesystem and returns the repository
func CopyBoilerplateModule(storage *memory.Storage, bfs *billy.Filesystem) *git.Repository {
	cloneOptions := &git.CloneOptions{
		URL: bitbucketBaseUrl + "/martin_kedmenec/internal-pipeline-boilerplate-module.git",
		Auth: &gogithttp.BasicAuth{
			Username: gitCredentials.Username,
			Password: gitCredentials.Password,
		},
	}

	repo, err := git.Clone(storage, *bfs, cloneOptions)
	internal.LogError(err)

	return repo
}

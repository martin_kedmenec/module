package git

import (
	"fmt"
	"time"

	"bitbucket.org/martin_kedmenec/module/internal"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	gogithttp "github.com/go-git/go-git/v5/plumbing/transport/http"
)

type RemoteInfo struct {
	CloneUrl string
	RepoUrl  string
}

// Initializes local and remote Git repositories
func InitGit(moduleDir string, workspace string, repositoryName string) {
	// Create a new Bitbucket remote repository
	rDone := make(chan *RemoteInfo)
	go func(rDone chan *RemoteInfo) {
		cloneUrl, repoUrl, err := createRemote(workspace, repositoryName)

		if err != nil {
			fmt.Println(err)
		}

		rDone <- &RemoteInfo{cloneUrl, repoUrl}
	}(rDone)

	// Initialize repository
	r, err := git.PlainInit(moduleDir, false)
	internal.LogError(err)

	// Get worktree
	w, err := r.Worktree()
	internal.LogError(err)

	remoteInfo := *<-rDone

	// Add remote
	err = addRemote(r, remoteInfo.CloneUrl)
	internal.LogError(err)

	// Add all files and directories in module to repository except the .git directory
	err = w.AddWithOptions(&git.AddOptions{
		All: true,
	})
	internal.LogError(err)

	// Parse ~/.gitconfig
	cfg := parseGitConfig()

	sgn := &object.Signature{
		Name:  cfg.User.Name,
		Email: cfg.User.Email,
		When:  time.Now(),
	}

	// Commit
	hash, err := w.Commit("Initial commit", &git.CommitOptions{
		Author: sgn,
	})
	internal.LogError(err)

	// Tag version 0.0.1
	_, err = r.CreateTag("0.0.1", hash, &git.CreateTagOptions{
		Tagger:  sgn,
		Message: "Initial commit",
	})
	internal.LogError(err)

	// Push to remote
	err = r.Push(&git.PushOptions{
		RemoteName: "origin",
		// RefSpecs:   []config.RefSpec{config.RefSpec("refs/tags/*:refs/tags/*")},
		Auth: &gogithttp.BasicAuth{
			Username: gitCredentials.Username,
			Password: gitCredentials.Password,
		},
	})
	internal.LogError(err)

	fmt.Println("Bitbucket repository created:", remoteInfo.RepoUrl)
}

package git

import (
	"encoding/base64"
	"net/http"

	"bitbucket.org/martin_kedmenec/module/internal"
)

// Makes a HTTP request to the Bitbucket API based on method and operation
func doRequest(httpMethod string, operation string, slug1 string, slug2 string) (*http.Response, error) {
	basicAuth := "Basic " + base64.StdEncoding.EncodeToString([]byte(gitCredentials.Username+":"+gitCredentials.Password))

	req, err := http.NewRequest(httpMethod, createApiUrl(operation, slug1, slug2), nil)
	internal.LogError(err)

	req.Header.Add("Authorization", basicAuth)

	client := &http.Client{}

	resp, err := client.Do(req)

	if err != nil {
		return nil, err
	}

	return resp, nil
}

package internal

import (
	"log"
)

// Checks if an error occured and panics if true
// func Panic(e error) {
// 	if e != nil {
// 		panic(e)
// 	}
// }

// Checks if an error occured and prints it if true
func LogError(e error) {
	if e != nil {
		log.Fatalln(e)
	}
}

// Checks if an error occured and prints it with a custom message if true
func LogMessageWithError(m string, e error) {
	if e != nil {
		log.Fatalln(m, e)
	}
}

// Checks if an error occured and returns it if true
func ReturnError(e error) error {
	if e != nil {
		return e
	}

	return nil
}

# Module

A CLI tool for creating Magento 2 Composer modules based on the internal pipeline boilerplate module. Works in conjunction with [this fork of the Internal Pipeline Boilerplate Module repository](https://bitbucket.org/martin_kedmenec/internal-pipeline-boilerplate-module/src/master/).

## How to use this tool

### Prerequisites

For this tool to work, two Git configuration files are necessary:

- `~/.gitconfig`
- `~/.git-credentials`

They need to be located in the specified (home) directory on your system.
